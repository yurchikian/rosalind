
f = open("datasets/rosalind_cons.txt")

nucs = "ACGT"

def readFasta(f):
    dnas = []
    dnabuff = ""
    for line in f.readlines():
        if line[0] == ">":
            if dnabuff != "":
                dnas.append(dnabuff)
                dnabuff = ""
        else:
            dnabuff += line.rstrip()
    dnas.append(dnabuff)
    return dnas

dnas = readFasta(f)

len = len(dnas[0])

# Calc Matrix:

dnam = {
    'A': [0] * len,
    'C': [0] * len,
    'G': [0] * len,
    'T': [0] * len
}

for j in dnas:
    for i in range(len):
        l = j[i]
        dnam[l][i] += 1

# Profile

li = ""

for i in range(len):
    max = 0
    maxnt = ""
    for nt in nucs:
        if dnam[nt][i] > max:
            max = dnam[nt][i]
            maxnt = nt
    li += maxnt

print li



for nt in nucs:
    print nt + ":",
    for j in dnam[nt]:
        print j,
    print ""


