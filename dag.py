f = open("datasets/rosalind_dag.txt")
graphsN = int(f.readline())

def performStep(node, edges, chain):
    ns = findNeighbours(node, edges)
    for n in ns:
        if n in chain:
            return True
        else:
            chain.append(n)
            res = performStep(n, edges, chain)
            if res:
                return res

    chain.remove(node)
    return False

def findNeighbours(node, edges):
    neighbours = []
    for p in edges:
        if p[0] == node:
            neighbours.append(p[1])
    return neighbours

def checkForAcyclicity(nodes, edges):
    for n in nodes:
        chain = [str(n)]
        res = performStep(str(n), edges, chain)
        if res:
            return res

    return False



for graphI in range(graphsN):
    f.readline()
    nums = f.readline().split()

    nodeN, edgeN = int(nums[0]), int(nums[1])

    nodes = range(1, nodeN + 1)
    edges = []

    for i in range(edgeN):
        edges.append(f.readline().split())

    if checkForAcyclicity(nodes, edges):
        print -1,
    else:
        print 1,


