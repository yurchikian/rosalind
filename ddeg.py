f = open("datasets/rosalind_ddeg.txt")
nums = f.readline().split()

nodes = int(nums[0])
edges = int(nums[1])

nodedegs = [0] * nodes

pairs = []

for i in range(edges):
    pair = f.readline().split()

    for p in pair:
        nodedegs[int(p) - 1] += 1

    pairs.append(pair)

def findNeighbours(node):
    neighbours = []
    for p in pairs:
        if p[0] == node:
            neighbours.append(p[1])
        elif p[1] == node:
            neighbours.append(p[0])
    return neighbours

for i in range(nodes):
    noden = findNeighbours(str(i + 1))
    sum = 0
    for n in noden:
        sum += nodedegs[int(n) - 1]
    print sum,
