## Rabbits and Recurrence Relations

f = open("datasets/rosalind_fib.txt")

numbers = f.readline().split()

n, k = int(numbers[0]), int(numbers[1])

def calcuateRabbits(m):
    if m == 0:
        return 0
    if m == 1:
        return 1
    else:
        return calcuateRabbits(m - 2) * k + calcuateRabbits(m - 1)

print calcuateRabbits(n)