## Counting Point Mutations

f = open("datasets/rosalind_hamm.txt")

dnas = f.readlines()

mutations = 0

for i in range(len(dnas[0]) - 1):
    if dnas[0][i] != dnas[1][i]:
        mutations += 1

print mutations