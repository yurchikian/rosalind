f = open("datasets/rosalind_deg.txt")
nums = f.readline().split()

nodes = int(nums[0])
edges = int(nums[1])

nodedegs = [0] * nodes

for i in range(edges):
    pair = f.readline().split()
    for p in pair:
        nodedegs[int(p) - 1] += 1

for n in nodedegs:
    print n,