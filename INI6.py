
f = open("datasets/rosalind_ini6.txt")
string = f.readline()

words = string.split()

d = {}
for v in words:
    if d.has_key(v):
        d[v] += 1
    else:
        d[v] = 1


for n in d:
    print n, d[n]