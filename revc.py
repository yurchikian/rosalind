## Complementing a Strand of DNA

f = open("datasets/rosalind_revc.txt")

aDNA = 'ACGT'
arDNA = 'TGCA'

dna = f.readline()
rdna = ""

for nt in dna:
    rdna = arDNA[aDNA.index(nt)] + rdna

print rdna