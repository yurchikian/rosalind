## Fibonacci Numbers

num = 25

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    elif n > 1:
        return fib(n - 1) + fib(n - 2)
    else:
        print "Error!"

print fib(num)
