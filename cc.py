f = open("datasets/rosalind_cc.txt")
nums = f.readline().split()

nodes = int(nums[0])
edges = int(nums[1])

pairs = []

for i in range(edges):
    pair = f.readline().split()
    pairs.append(pair)

def findNeighbours(node):
    neighbours = []
    for p in pairs:
        if p[0] == node:
            neighbours.append(p[1])
        elif p[1] == node:
            neighbours.append(p[0])
    return neighbours


nodesy = []

for i in range(nodes):
    nodesy.append(i + 1)

nodelists = []

def pushFriendsToNodelistRecursively(node, nodelist):
    ns = findNeighbours(node)
    if node in nodelist:
        return
    else:
        nodelist.append(node)
        if int(node) in nodesy:
            nodesy.remove(int(node))
        # print node
        for n in ns:
            pushFriendsToNodelistRecursively(n, nodelist)



for i in range(nodes):
    if i + 1 in nodesy:
        nodelist = []
        pushFriendsToNodelistRecursively(str(i + 1), nodelist)
        nodelist.sort()
        if not nodelist in nodelists:
            nodelists.append(nodelist)

print len(nodelists)