## Transcribing DNA into RNA

f = open("datasets/rosalind_rna.txt")

aDNA = 'ACGT'
aRNA = 'ACGU'

dna = f.readline()
rna = ""

for nt in dna:
    rna += aRNA[aDNA.index(nt)]

print rna