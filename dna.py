## Counting DNA Nucleotids

f = open("datasets/rosalind_dna.txt")
row = 'ACGT'

dna = f.readline()

dnac = {
    'A': 0,
    'C': 0,
    'G': 0,
    'T': 0
}

for l in dna:
    dnac[l] += 1

for l in row:
    print dnac[l],