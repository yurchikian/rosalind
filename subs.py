## Finding a Motif in DNA

f = open("datasets/rosalind_subs.txt")

dna = f.readline()
mot = f.readline()

l = len(mot)

for i in range(0, len(dna) - l):
    if dna[ i : i + l ] == mot:
        print i + 1,