f = open("datasets/rosalind_bins.txt")

n = int(f.readline())
m = int(f.readline())

A = f.readline().split()
L = f.readline().split()

def whichPart(num, arr, fr, to):
    l = to - fr
    if l == 1:
        if int(arr[fr]) == num:
            return fr
        else:
            return -2
    else:
        if num >= int(arr[fr + l / 2]):
            return whichPart(num, arr, fr + l / 2, to)
        else:
            return whichPart(num, arr, fr, to - l / 2)

for number in L:
    print whichPart(int(number), A, 0, len(A) - 1) + 1,